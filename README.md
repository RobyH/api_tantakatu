# Proyecto Final Curso Rest - Tantakatu
## GRUPO: THE ROCKS

_**Docente:**_
* Ing. Javier Roca

_**Maestrantes:**_

* Juan Pablo Bascope Castro

<img src="IMAGES/team-1.jpg" width="100">

* Alejandra Pamela Sivila Padilla

<img src="IMAGES/team-2.jpg" width="100">

* Cesar Roberto Herbas Delgadillo

<img src="IMAGES/team-3.jpg" width="100">

* Juan Carlos Ortube

<img src="IMAGES/team-4.jpg" width="100">

### Especificación del proyecto:
* Diseño del API (Swagger o RAML o equivalente)
* BMC del API (Dominios via DDD es un plus)
* Documentación y Mock
* Detalle breve de la política de autenticacion y autorizacion utilizada
* Dirección GIT del proyecto
* URL para el acceso al proyecto vía REST
* Estrategia de escalabilidad (razonamiento y explicación/evidencias de pruebas realizadas)
* Ilustración práctica de los flujos del API (ej. vía Postman, un app Web, un testing framework como jasmine.js). La idea es poder demostrar y evidenciar la implementación de todos los elementos del API. 
* Mínimamente el API debe implementar:
 - Transacciones de Compra/Venta de Artículos
 - Registro de transacciones de Compra y Venta
 - Notificaciones cuando se realiza una transacción (e.j. se acepta que escriba un log file simulando una notificación).
 - CRUD de Artículos, Usuarios, Categorías
 - Busqueda de Articulos, Usuarios, Categorías

## Desarrollo del proyecto


Repositorio Git: https://gitlab.com/RobyH/api_tantakatu

Documentacion Api: http://200.107.241.6:3000/docs/ 
                   https://documenter.getpostman.com/view/4249844/tantakatu/RW8CJo4S
Url Mock: Url Aplicacion: https://mocksvc.mulesoft.com/mocks/d6cfe4a9-9a88-48ce-b099-6935b4de8599

Url Aplicacion: http://200.107.241.6:8080 (Aun en proceso)


_**Herramientas de comunicacion y desarrollo:**_
* Google drive: https://docs.google.com/document/d/1hIEP8DKNYIJ2RcooiTV8kaYLdl3Y17z7xXlB7eLWXkA/edit?usp=sharing
* Slack: https://join.slack.com/t/therock-apirest/shared_invite/enQtMzcwNDcwMDYxOTg1LTkwY2ZkNjVkYzdmNTg2MzNmNjE2M2M1ZTU0MjAyNDFkYjRkZGU1NDgxMGU2NTU0YzAyYTU5MjgzYTJlMWIyNmI
* Hangouts

_**Diseño de los casos de uso:**_

<img src="IMAGES/usecase.png">

_**Modelo entidad relación:**_
* Aclaración de tipo de usuario: Se toma a usuarios como AdministradorApi, ClienteTienda, ClienteComprador, ClienteVendedor, ClienteAnunciante

<img src="IMAGES/mer.png">

_**Estructura Microservicios:**_

<img src="IMAGES/ms.png">

_**Flows tipos de usuario del Api:**_

* Flujo administrador_api

<img src="IMAGES/flowAdministratorApi.png">

* Flujo cliente_tienda, cliente_compra, cliente_venta, cliente_anuncio

<img src="IMAGES/flowClientes.png">

_**Portada principal de la documentacion API:**_

<img src="IMAGES/portadaDocumentacion.png">

