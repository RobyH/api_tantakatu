"use strict";

const config = {
    hostname: "127.0.0.1",
    port: 3000,
    db: {
        url: "mongodb://localhost/db_service_products"
    },
    JWT_SECRET: "rZVoZtLR4SnPxy8M9eILZ9AnLmysO8oW"
};

module.exports = config;