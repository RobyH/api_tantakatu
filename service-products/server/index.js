"use strict";

const express = require('express');
const morgan = require('morgan');
const logger = require('winston');
const bodyParser = require('body-parser');
const mongoose = require("mongoose");
const bluebird = require('bluebird');

const config = require("./config")

// Setup router and routes
const api = require("./api/v1");
// Use bluebird
mongoose.Promise = bluebird;
// Connect to database
mongoose.connect(config.db.url);

const app = express();
// Setup middleware
app.use(morgan('common'));
// parse application/x-www-form-urlencoded 
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json 
app.use(bodyParser.json());
// Setup router and routes
app.use("/api", api);
app.use("/api/v1", api);

app.get('/', (req, res, next) => {
  res.json({
    "message": "Bienvenido al servicio productos"
  });
});

app.use( (req, res, next) => {
  res.status(404);
  res.json({
    "error": "Error. Routa no encontrada"
  });
});

app.use( (err, req, res, next) => {
  res.status(500);
  res.json({
    "error": `${err}`
  });
});

module.exports = app;