"use strict";

const router = require("express").Router();
const controller = require("./../controllers/products");

/*
 * /api/products/		GET    - READ ALL
 * /api/products/		POST   - CREATE
 * /api/products/:id 	GET    - READ ONE
 * /api/products/:id 	PUT    - UPDATE
 * /api/products/:id 	DELETE - DELETE
*/

router.param("id", controller.params);

router.route("/")
    .get(controller.all)
    .post(controller.post);

router.route("/:id")
    .get(controller.get)
    .put(controller.put)
    .delete(controller.delete);

module.exports = router;