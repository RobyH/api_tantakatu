"use strict";

const logger = require("winston");
const _ = require("lodash");
const Product = require("./../schemas/products");

exports.params = (req, res, next, id) => {
  Product.findById(id)
    .populate("category")
    .exec()
    .then( product => {
      if (product) {
        req.product = product;
        next();
      } else {
        res.json({
            "message": "Producto no encontrado"
        });
      }
    })
    .catch( err => {
      next(new Error(err));
    });
};

exports.all = (req, res, next) => {
  Product.find()
    .then( products => {
      res.json(products);
    })
    .catch( err => {
      next(new Error(err));
    });
};

exports.post = (req, res, next) => {
  let body = req.body;

  const product = new Product(body);
  
  product.save()
    .then( newproduct => {
        res.json(newproduct);
    })
    .catch( err => {
        next(new Error(err));
    });
};

exports.get = (req, res, next) => {
  const product = req.product;

  res.json(product);
};

exports.put = (req, res, next) => {
  const body = req.body;
  
  const product = _.merge(req.product, body);

  product.save()
    .then( updated => {
      res.json(updated);
    })
    .catch( err => {
      next(new Error(err));
    });
};

exports.delete = (req, res, next) => {
  const product = req.product;
  
  product.remove()
    .then(removed => {
      res.json(removed);
    })
    .catch( err => {
      next(err);
    });
};