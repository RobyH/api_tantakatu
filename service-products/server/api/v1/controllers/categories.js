"use strict";

const logger = require("winston");
const _ = require("lodash");
const Category = require("./../schemas/categories");
const Product = require("./../schemas/products");

exports.params = (req, res, next, id) => {
  Category.findById(id)
    .then( category => {
      if (category) {
        req.category = category;
        next();
      } else {
        res.json({ "message": "Categoria no encontrada" });
      }
    })
    .catch( err => {
      next(new Error(err));
    });
};

exports.all = (req, res, next) => {
  Category.find()
    .then( categories => {
      res.json(categories);
    })
    .catch( err => {
      next(new Error(err));
    });
};

exports.post = (req, res, next) => {
  let body = req.body;

  const category = new Category(body);
  
  category.save()
    .then( newcategory => {
        res.json(newcategory);
    })
    .catch( err => {
        next(new Error(err));
    });
};

exports.get = (req, res, next) => {
  const category = req.category;

  res.json(category);
};

exports.put = (req, res, next) => {
  const body = req.body;
  
  const category = _.merge(req.category, body);

  category.save()
    .then( updated => {
      res.json(updated);
    })
    .catch( err => {
      next(new Error(err));
    });
};

exports.delete = (req, res, next) => {
  const category = req.category;
  
  category.remove()
    .then(removed => {
      res.json(removed);
    })
    .catch( err => {
      next(err);
    });
};

exports.products = (req, res, next) => {
  const id = req.params.id;

  Product.find({category: id})
    .then( products => {
      res.json(products);
    })
    .catch( err => {
      next(new Error(err));
    });
};