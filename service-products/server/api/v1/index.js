"use strict";

const router = require("express").Router();

const categoriesRoutes = require("./routes/categories");
const productsRoutes = require("./routes/products");

const auth = require('./middlewares/auth')
//const auth2 = require('./middlewares/authenticate')

router.use("/categories", auth, categoriesRoutes);
router.use("/products", productsRoutes);

module.exports = router;