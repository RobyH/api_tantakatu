'use strict'

const jwt = require('jsonwebtoken')
const config = require("./../../../config")

function isAuth(req, res, next)
{
  if(!req.headers.authorization)
  {
    return res.status(403).send({
      status: false,
      message: 'Acceso no autorizado'
    })
  }

  const token = req.headers.authorization
  //const token = req.headers.authorization.split(" ")[1];
  //token = token.replace('Bearer ', '')

  jwt.verify(token, config.JWT_SECRET, function(err, token) {
    if (err) {
      return res.status(401).send({
        status: false,
        message: 'Token inválido',
        token: req.headers.authorization
      });
    }
    else {
      req.userId = token.sub
      //global.userId = token.sub
      next()
    }
  });
}

module.exports = isAuth