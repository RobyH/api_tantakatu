<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Juan Pablo Bascope',
            'email' => 'pabloblass@gmail.com',
            'password' => app('hash')->make('123456'),
            'address' => 'Manuel Molina #76',
            'cellphone' => '76219846',
            'created_at'    => Carbon::now(),
            'updated_at'    => Carbon::now()
        ]);

        factory(App\User::class, 20)->create();
    }
}
