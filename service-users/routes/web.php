<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function () use ($router)
{
  $router->post('/auth/login', 'AuthController@login');
  $router->post('users', ['uses' => 'UserController@create']);

	// Para las siguientes rutas se aplica el middleware porque se debe enviar el token
  $router->group(['middleware' => 'auth:api'], function () use ($router)
	{
	    $router->post('/auth/logout', 'AuthController@logout');
	    $router->put('/auth/refresh', 'AuthController@refresh');
	    $router->get('users',  ['uses' => 'UserController@index']);
	    $router->get('users/{id}', ['uses' => 'UserController@show']);
	    $router->put('users/{id}', ['uses' => 'UserController@update']);
	    $router->delete('users/{id}', ['uses' => 'UserController@delete']);
	});
});


