<?php

namespace App\Http\Controllers;

use App\User;
use Tymon\JWTAuth\JWTAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use Illuminate\Support\Facades\Auth;
//use Tymon\JWTAuth\Contracts\Providers\Auth;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthController extends Controller
{
  /**
   * @var \Tymon\JWTAuth\JWTAuth
   */
  protected $jwt;

  public function __construct(JWTAuth $jwt)
  {
      $this->jwt = $jwt;
  }

  public function login(Request $request)
  {
    $this->validate($request, [
        'email'    => 'required|email|max:150',
        'password' => 'required|min:6',
    ]);
    
    $credentials = $request->only('email', 'password');
    //$customClaims = ['name' => $user->name];

    try {
        //if (! $token = $this->jwt->attempt($credentials, $customClaims)) {
        if (! $token = $this->jwt->attempt($credentials)) {
            return response()->json(['usuario_o_contraseña_incorrectos'], 401);
        }

    } catch (TokenExpiredException $e) {
        return response()->json(['token_expirado'], 500);
    } catch (TokenInvalidException $e) {
        return response()->json(['token_invalido'], 500);
    } catch (JWTException $e) {
        return response()->json(['token_no_creado' => $e->getMessage()], 500);
    }
    return response()->json(compact('token'));
    /*$user = \Auth::user();
    return response()->json([
        'token' => $token,
        'user' => $user,
    ]);*/
  }

  public function logout()
  {
    try {
      $token = $this->jwt->getToken();
      $this->jwt->invalidate($token);
    } catch (TokenInvalidException $e) {
        return response()->json(['token_invalido'], 500);
    }

    return response()->json(['message' => 'Logout correcto'], 200);
  }

  public function refresh()
  {
    try {
      $token = $this->jwt->getToken();
      $token = $this->jwt->refresh($token);
    } catch (TokenInvalidException $e) {
        return response()->json(['token_invalido'], 500);
    }
    
    return response()->json(compact('token'));
  }

  /*public function refresh()
  {
    try {
      return $this->json([
          'token' => $this->manager->refresh($this->jwt->getToken())->get()
      ]);

    } catch (JWTException $e) {
      return $this->json($e->getMessage(), 500);
    }
  }*/
}