<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function index()
  {
    try{
      $result = ['status' =>true, 'users' => [] ];
      $users = User::all();
      if($users)
        $result = ['status' =>true, 'users' => $users ];
        
      return response()->json($result, 200);
    }
    catch(\Exception $e){
      $result = ['status' =>false, 'message' => $e->getMessage() ];
      return response()->json($result, 500);
    }

    
  }

  public function show($id)
  {
    try{
      $user = User::find($id);

      if($user) {
        $result = ['status' => true, 'user' => $user ];
        return response()->json($result, 200);
      }
      else {
        $result = ['status' => false, 'message' => 'Usuario no encontrado' ];
        return response()->json($result, 404);
      }
    }
    catch(\Exception $e){
      $result = ['code' => 500, 'status' =>false, 'data' => $e->getMessage() ];
    }

    return response()->json($result);
  }

  public function create(Request $request)
  {
    $rules = [
      'name' => 'required|string|max:150',
      'email' => 'required|string|email|max:150|unique:users',
      'password' => 'required|string|min:6|max:255|confirmed',
      'cellphone' => 'string|max:15',
      'address' => 'string|max:200',
      'city' => 'string|max:100',
      'province' => 'string|max:100',
      'country' => 'string|max:100',
     ];

    $customMessages = [
      'required' => 'El campo :attribute es obligatorio.'
    ];
    $this->validate($request, $rules, $customMessages);
    try{
      $user = new User;
      $user->name = $request->get('name');
      $user->email = $request->get('email');
      $user->password = app('hash')->make($request->get('password'));
      $user->cellphone = $request->get('cellphone');
      $user->address = $request->get('address');
      $user->city = $request->get('city');
      $user->province = $request->get('province');
      $user->country = $request->get('country');
      $user->save();
      $result = ['status' =>true, 'message' => 'Registro correcto', 'user' => $user ];
      return response()->json($result, 201);
    }
    catch(\Exception $e){
      $result = array('status' =>false, 'message' => $e->getMessage() );
      return response()->json($result, 500);
    }
    
    
  }

  public function update($id, Request $request)
  {
    $rules = [
      'name' => 'required|string|max:150',
      'email' => 'required|string|email|max:150|unique:users',
      'password' => 'required|string|min:6|max:255|confirmed',
      'cellphone' => 'string|max:15',
      'address' => 'string|max:200',
      'city' => 'string|max:100',
      'province' => 'string|max:100',
      'country' => 'string|max:100',
     ];

    $customMessages = [
      'required' => 'El campo :attribute es obligatorio.'
    ];
    $this->validate($request, $rules, $customMessages);
    try{
      $user = User::find($id);
      if($user) {
        $user->name = $request->get('name');
        $user->email = $request->get('email');
        $user->password = app('hash')->make($request->get('password'));
        $user->cellphone = $request->get('cellphone');
        $user->address = $request->get('address');
        $user->city = $request->get('city');
        $user->province = $request->get('province');
        $user->country = $request->get('country');
        $user->update();
        $result = ['status' =>true, 'message' => 'Registro correcto', 'user' => $user ];
        return response()->json($result, 201);
      }
      else {
        $result = ['status' =>false, 'message' => 'Usuario no encontrado' ];
        return response()->json($result, 404);
      }
    }
    catch(\Exception $e){
      $result = array('status' =>false, 'message' => $e->getMessage() );
      return response()->json($result, 500);
    }

    return response()->json($result, 201);
  }

  public function delete($id)
  {
    try{
      $user = User::find($id);

      if($user) {
        $user->delete();
        $result = ['status' =>true, 'message' => 'Eliminacion correcta' ];
        return response()->json($result, 204);
      }
      else {
        $result = ['status' =>false, 'message' => 'Usuario no encontrado' ];
        return response()->json($result, 404);
      }
    }
    catch(\Exception $e){
      $result = array('status' =>false, 'message' => $e->getMessage() );
      return response()->json($result, 500);
    }
  }
}
