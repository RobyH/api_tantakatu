var express = require('express');
var cors = require('cors');
var bodyParser = require("body-parser");
var app = express();
var port = process.env.PORT || 3001;

app.use(bodyParser.json());
app.use(cors());

app.use(bodyParser.urlencoded({
    extended: false
}));

var Users = require('./Routes/Users');
var Client_sales = require('./Routes/Client_sales');
var Client_shops = require('./Routes/Client_shops');
var Client_Purchases = require('./Routes/Client_purchases');
var Client_adwords = require('./Routes/Client_adwords');


app.use('/users',Users);
app.use('/client_sales',Client_sales);
app.use('/client_shops',Client_shops);
app.use('/client_purchases',Client_Purchases);
app.use('/client_adwords',Client_adwords);

app.listen(port,function(){
    console.log("Microservicio Usuarios, Corriendo en el puerto: "+port);
});