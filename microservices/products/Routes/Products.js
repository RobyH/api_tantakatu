'use strict';

var express = require('express');
var products = express.Router();
var database = require('../Database/database');
//Lib JWT
var cors = require('cors')
var jwt = require('jsonwebtoken');
//Generate LOGS
var log4js = require('log4js');
log4js.configure({
    appenders: {user: {type: 'file', filename: 'logs/user.log'}},
    categories: {default: {appenders: ['user'], level: 'info'}}
});
var logger = log4js.getLogger('user');
var token;
products.use(cors());
process.env.SECRET_KEY = "carlitos2018";
//Canal para el envio de la cola 


// Verificacion usuario

// Verificacion del token middleware
products.use(function (req, res, next) {
    var token = req.body.token || req.headers['token'];
    var email = req.body.email;
    var jsonData = {};
    if (token) {
        jwt.verify(token, process.env.SECRET_KEY, function (err) {
            if (err) {
                jsonData["error"] = 1;
                jsonData["data"] = "El Token es invalido";
                logger.info('TOKEN INVALIDO! LOGIN U: [' + email + '] S: [ERROR]');
                res.status(500).json(jsonData);
            } else {
                next();
            }
        });
    } else {
        jsonData["error"] = 1;
        jsonData["data"] = "Please send a token";
        logger.info('TOKEN INVALIDO! LOGIN U: [' + email + '] S: [ERROR]');
        res.status(403).json(jsonData);
    }
});
// Nuevo producto
products.post('/', function (req, res) {
    var token = req.body.token || req.headers['token'];
    var decoded = jwt.decode(token, process.env.SECRET_KEY);
    var tipo_usuario = decoded['tipo_usuario'];
    var jsonData = {
        "error": 1,
        "data": ""
    };
    if (tipo_usuario == 'client_sales')
    {
        var id_cliente = decoded['id'];
        var today = new Date();

        var productsData = {
            "id_cliente_venta": id_cliente,
            "id_categoria": req.body.id_categoria,
            "nombre": req.body.nombre_producto,
            "descripcion": req.body.descripcion,
            "precio": req.body.precio,
            "fecha_reg": today,
            "estado": 1
        }
        database.connection.getConnection(function (err, connection) {
            if (err) {
                jsonData["error"] = 1;
                jsonData["data"] = "Error interno de servidor";
                logger.info('ERROR AL CONECTAR AL SERVIDOR [ERROR]');
                res.status(500).json(jsonData);
            } else {
                connection.query('INSERT INTO producto SET ?', productsData, function (err, rows, fields) {
                    if (!err) {
                        jsonData.error = 0;
                        jsonData["data"] = "Usuario registrado exitosamente!";
                        logger.info('USUARIO CREADO EXITOSAMENTE U: [' + productsData['email'] + '] S: [SUCCESS]');
                        res.status(201).json(jsonData);
                    } else {
                        jsonData["data"] = err;
                        logger.info('ERROR AL CREAR EL USUARIO U: [' + productsData['email'] + '] S: [ERROR]');
                        res.status(400).json(jsonData);
                    }
                });
                connection.release();
            }
        });
    } else {
        jsonData["data"] = "Usuario no Autorizado";
        logger.info('ERROR AL CREAR EL USUARIO  S: [ERROR]');
        res.status(400).json(jsonData);
    }
});
// Se obtienen todos los usuarios SOLO PARA ADMINISTRADORES 
products.get('/', function (req, res) {
    var jsonData = {};
    var type_user = req.params.type_user;
    var token = req.body.token || req.headers['token'];
    var decoded = jwt.decode(token, process.env.SECRET_KEY);
    var login = decoded['email'];
    var tipo_usuario = decoded['tipo_usuario'];
 //   if (tipo_usuario == "administrador_api")
   // {
        database.connection.getConnection(function (err, connection) {
            if (err) {
                jsonData["error"] = 1;
                jsonData["data"] = "Error interno de servidor";
                res.status(500).json(jsonData);
            } else {
                connection.query('SELECT * FROM producto', function (err, rows, fields) {
                    if (!err) {
                        jsonData["error"] = type_user;
                        jsonData["data"] = rows;
                        logger.info('SE OBTIENE REGISTROS DE USUARIOS CORRECTAMENTE: LOGIN U: [' + login + '][SUCCESS]');
                        res.status(200).json(jsonData);
                    } else {
                        jsonData["data"] = "No hay datos";
                        logger.info('ERROR AL OBTENER REGISTROS DE USUARIOS: LOGIN U: [' + login + '][ERROR]');
                        res.status(400).json(jsonData);
                    }
                });
                connection.release();
            }
        });
  //  } else {
      //  jsonData["data"] = "Usuario no autorizado";
      //  logger.info('ERROR Usurio no autorizado LOGIN U: [' + login + '][ERROR]');
      //  res.status(400).json(jsonData);
  //  }
});
products.get('/categories', function (req, res) {
    var jsonData = {};
    var type_user = req.params.type_user;
    var token = req.body.token || req.headers['token'];
    var decoded = jwt.decode(token, process.env.SECRET_KEY);
    var login = decoded['email'];
    var tipo_usuario = decoded['tipo_usuario'];
    if (tipo_usuario == "administrador_api")
    {
        database.connection.getConnection(function (err, connection) {
            if (err) {
                jsonData["error"] = 1;
                jsonData["data"] = "Error interno de servidor";
                res.status(500).json(jsonData);
            } else {
                connection.query('SELECT * FROM categoria', function (err, rows, fields) {
                    if (!err) {
                        jsonData["error"] = type_user;
                        jsonData["data"] = rows;
                        logger.info('SE OBTIENE REGISTROS DE USUARIOS CORRECTAMENTE: LOGIN U: [' + login + '][SUCCESS]');
                        res.status(200).json(jsonData);
                    } else {
                        jsonData["data"] = "No hay datos";
                        logger.info('ERROR AL OBTENER REGISTROS DE USUARIOS: LOGIN U: [' + login + '][ERROR]');
                        res.status(400).json(jsonData);
                    }
                });
                connection.release();
            }
        });
    } else {
        jsonData["data"] = "Usuario no autorizado";
        logger.info('ERROR Usurio no autorizado LOGIN U: [' + login + '][ERROR]');
        res.status(400).json(jsonData);
    }
});
products.get('/categories/:id', function (req, res) {
    var id_categoria = req.params.id;
    var jsonData = {};
    var type_user = req.params.type_user;
    var token = req.body.token || req.headers['token'];
    var decoded = jwt.decode(token, process.env.SECRET_KEY);
    var login = decoded['email'];
    var tipo_usuario = decoded['tipo_usuario'];
    if (tipo_usuario == "administrador_api")
    {
        database.connection.getConnection(function (err, connection) {
            if (err) {
                jsonData["error"] = 1;
                jsonData["data"] = "Error interno de servidor";
                res.status(500).json(jsonData);
            } else {
                connection.query('SELECT * FROM producto where id_categoria = ?', id_categoria , function (err, rows, fields) {
                    if (!err) {
                        jsonData["error"] = type_user;
                        jsonData["data"] = rows;
                        logger.info('SE OBTIENE REGISTROS DE USUARIOS CORRECTAMENTE: LOGIN U: [' + login + '][SUCCESS]');
                        res.status(200).json(jsonData);
                    } else {
                        jsonData["data"] = "No hay datos";
                        logger.info('ERROR AL OBTENER REGISTROS DE USUARIOS: LOGIN U: [' + login + '][ERROR]');
                        res.status(400).json(jsonData);
                    }
                });
                connection.release();
            }
        });
    } else {
        jsonData["data"] = "Usuario no autorizado";
        logger.info('ERROR Usurio no autorizado LOGIN U: [' + login + '][ERROR]');
        res.status(400).json(jsonData);
    }
});
// Se obtienen todos los usuarios
products.delete('/:id', function (req, res) {
    var jsonData = {};
    var token = req.body.token || req.headers['token'];
    var decoded = jwt.decode(token, process.env.SECRET_KEY);
    var login = decoded['email'];
    var id_users = decoded['id_user'];
    var idDelete = req.params.id;
    database.connection.getConnection(function (err, connection) {
        if (err) {
            jsonData["error"] = 1;
            jsonData["data"] = "Error interno de servidor";
            res.status(500).json(jsonData);
        } else {
            connection.query('DELETE FROM usuarios WHERE id = ?', [idDelete], function (err, result) {
                if (!err) {
                    jsonData["error"] = 0;
                    jsonData["data"] = "Usuario Eliminado Correctamente";
                    logger.info('REGISTRO ELIMINADO CORRECTAMENTE: LOGIN U: [' + login + '][SUCCESS]');
                    var id_u = req.params.id;
                    res.status(200).json(jsonData);
                } else {
                    jsonData["data"] = "No hay datos";
                    logger.info('ERROR AL ELIMINAR REGISTRO DE USUARIO: LOGIN U: [' + login + '][ERROR]');
                    res.status(400).json(jsonData);
                }
            });
            connection.release();
        }
    });
});
// Se obtienen todos los usuarios
products.put('/:id', function (req, res) {
    var jsonData = {};
    var decoded = jwt.decode(token, process.env.SECRET_KEY);
    var login = decoded['email'];
    var idUser = req.params.id;
    var first_name = req.body.first_name;
    var last_name = req.body.last_name;
    var email = req.body.email;
    var password = req.body.password;
    var tipo_users = req.body.tipo_users;
    database.connection.getConnection(function (err, connection) {
        if (err) {
            jsonData["error"] = 1;
            jsonData["data"] = "Error interno de servidor";
            res.status(500).json(jsonData);
        } else {
            connection.query('UPDATE users SET first_name = ? , last_name = ? , email = ? , password = ? , tipo_users = ? WHERE id = ?', [first_name, last_name, email, password, tipo_users, idUser], function (err, result) {
                if (!err) {
                    jsonData["error"] = 0;
                    jsonData["data"] = "Usuario Modificado exitosamente!";
                    logger.info('REGISTRO ACTUALIZADO CORRECTAMENTE: LOGIN U: [' + login + '][SUCCESS]');
                    res.status(200).json(jsonData);
                } else {
                    jsonData["data"] = "Error al momento de actualizar usuario";
                    logger.info('ERROR AL ACTUALIZAR REGISTRO DE USUARIO: LOGIN U: [' + login + '][ERROR]');
                    res.status(400).json(jsonData);
                }
            });
            connection.release();
        }
    });
});


module.exports = products;